import React, { useEffect, useState } from "react";
import { ThemeProvider } from "styled-components";
import { GlobalStyles, globalStyles } from "../components/globalStyles";
import { lightTheme, darkTheme } from "../components/theme";
import id from "../../public/photoID.jpg";

export default function Welcome() {
	return (
		<div className="container-welcome">
			<img src={id} alt="photo identité" />
			<h1>Concepteur / Développeur d'applications</h1>
			<span role="img" aria-label="emoji-round-pushpin">
				📍&nbsp; Bordeaux, France
			</span>
			<div>
				<h2>Développeur Web Full-Stack</h2>
				<h3>
					<span role="img" aria-label="emoji-open-book">
						📖
					</span>
					&nbsp; Bio
				</h3>
				<p>
					En reconversion depuis 2018, j'ai tout d'abord fait une formation à la{" "}
					<a>3WAcademy</a> de Paris comme développeur d'applications IOS
				</p>
				<p>
					Après avoir découvert le développement en SWIFT, j'ai voulu apprendre
					des langages plus généralistes comme le C# ou le JavaScript
				</p>
				<p>
					J'ai pu intégrer l' <a>EPSI Bordeaux</a> quelques mois plus tard pour
					réaliser ma première expérience en entreprise chez <a>CDiscount</a> en
					alternance
				</p>
				<p>
					J'ai pu lors de cette expérience pratiquer professionnellement les
					framework ReactJS/ .NEt / .NEt Core ainsi qu'apprendre à utiliser
					Talend (E.T.L.) et manipuler les BDD en Transact-SQL sur le SGBD SQL
					Server
				</p>
				<p>
					Depuis, je monte en compétences sur des projets personnels,
					majoritairement en ReactJS avec notamment le framework{" "}
					<a href="https://www.gatsbyjs.org/">Gatsby</a>
				</p>
				<h3> 📋 Ma CheckList</h3>
				<ul aria-label="JavaScript">
					<li>✅ Gatsby</li>
					<li>🗹 Gatsby</li>
					<li>☐ Vue JS</li>
				</ul>
			</div>
		</div>
	);
}
