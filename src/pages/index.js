import Header from "../templates/header";
import Layout from "../components/layout";
import { ThemeProvider } from "styled-components";
import { GlobalStyles } from "../components/globalStyles";
import { lightTheme, darkTheme } from "../components/theme";
import React, { useEffect, useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import AboutPage from "./about";
import Welcome from "./welcome";
import Footer from "../templates/footer";
import Actions from "../components/actions";

export default function Home() {
	const [kolor, setColor] = useState("#66CDAA");
	const [theme, setTheme] = useState("light");
	const themeToggler = () => {
		theme === "light" ? setTheme("dark") : setTheme("light");
		console.log(theme);
	};
	return (
		<ThemeProvider theme={theme === "light" ? lightTheme : darkTheme}>
			<>
				<Router>
					<GlobalStyles />
					<Header toggle={themeToggler} colour={kolor} />
					<Layout>
						<Switch>
							<Route exact path="/" render={() => <Welcome />} />
							<Route path="/about" render={() => <AboutPage />} />
						</Switch>
						<Actions onChangeColor={setColor} color={kolor} />
					</Layout>
					<Footer />
				</Router>
			</>
		</ThemeProvider>
	);
}
