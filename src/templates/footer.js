import React from "react";

export default function footer() {
	return (
		<div>
			<h1>Me contacter</h1>
			<a
				href="mailto:gregoire.falquerho@gmail.com"
				target="_blank"
				rel="noopener noreferrer"
			>
				m'envoyer un email
			</a>
			<h1>Me trouver</h1>
			<h3>Logo LK</h3>
		</div>
	);
}
