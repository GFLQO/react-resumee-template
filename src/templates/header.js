import { Link } from "gatsby";
import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAdjust } from "@fortawesome/free-solid-svg-icons";

export default function Header(props) {
	return (
		<div className="header-container">
			<div className="header-title">
				<div>
					<h1 style={{ color: `${props.colour}` }}>
						Grégoire Falquerho' Web Dev
					</h1>
					<i style={{ padding: "3px" }}>
						<code>@GFLQO</code>
					</i>
				</div>
			</div>
			<div></div>
			<div></div>
			<div></div>
			<div></div>
			<nav>
				<ul>
					<li>
						<Link to="/">Home</Link>
					</li>
					<li>
						<Link to="/about">My Work</Link>
					</li>
					<li>
						<Link to="/">Contact</Link>
					</li>
				</ul>
			</nav>
			<div onClick={props.toggle} className="dark-icon">
				<FontAwesomeIcon icon={faAdjust} size="2x" />{" "}
			</div>
		</div>
	);
}
