import React from "react";

export default function PetTemplates({ pageContext: { pet } }) {
	return (
		<section>
			{pet.name} - {pet.breed}
		</section>
	);
}
