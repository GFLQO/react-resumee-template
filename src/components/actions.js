import React, { useState } from "react";
import { HexColorPicker } from "react-colorful";
import "react-colorful/dist/index.css";

export default function Actions(props) {
	const [toggle1, setToggle1] = useState("block");

	const shouldBeShown = () => {
		setToggle1(!toggle1);
	};
	return (
		<div>
			<h1>Let's play</h1>
			<div>
				<div
					onClick={shouldBeShown}
					style={{
						padding: "15px",
						height: "auto",
					}}
				>
					<span
						style={{
							backgroundColor: `${props.color}`,
							borderRadius: "50%",
							padding: "5px",
							paddingLeft: "8px",
							cursor: "pointer",
						}}
					>
						<i className="fas fa-tint"></i>{" "}
					</span>
					&nbsp;
					<span>Change la couleur du Titre </span>
				</div>
				{toggle1 ? (
					<div style={{ display: `${toggle1}` }}>
						<HexColorPicker
							color={props.color}
							onChange={props.onChangeColor}
						/>
					</div>
				) : null}
			</div>
		</div>
	);
}
