exports.createPages = ({ actions }) => {
	const { createPage } = actions;

	const dogData = [
		{
			name: "Fido",
			breed: "sheltie",
		},
		{
			name: "Rififi",
			breed: "Kitty",
		},
	];

	dogData.forEach((pet) => {
		createPage({
			path: `/${pet.name}`,
			component: require.resolve(`./src/templates/pet-templates.js`),
			context: { pet },
		});
	});
};
